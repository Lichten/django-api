**Модуль для подписок, друзей и т.д.**

django-friendship - https://pypi.org/project/django-friendship/


**Установка всех модулей в виртуальном окружении**
`pipenv install`

Для этого необходимо установить pipenv
`pip install pipenv`


**Необходимо сделать миграции в базу данных django:**
`python manage.py migrate`

**БД**: Postgresql
**Настройки подключения**:
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
```