from social.forms import RegisterForm, LoginForm
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from friendship.models import Friend, FriendshipRequest


def index(request):
    return render(request, 'index.html')


def register(request):
    form = RegisterForm()

    if request.method == "POST":
        form = RegisterForm(data=request.POST)

        if form.is_valid():
            form.save()
            return redirect(reverse("login"))

    return render(
        request,
        'register.html',
        {
            'form': form
        }
    )


@login_required(login_url='login')
def all_users(request):
    users = User.objects.all()
    friends = Friend.objects.friends(request.user)

    return render(
        request,
        'users.html',
        {
            'users': users,
            'friends': friends
        }
    )


@login_required
def add_friend(request, username):
    user = User.objects.get(username=username)
    if request.user != user:
        Friend.objects.add_friend(request.user, user)
        friendfhip = FriendshipRequest.objects.get(to_user=user)
        friendfhip.accept()
        messages.success(request, 'Друг добавлен!')
        return redirect(reverse('users'))

    return redirect(reverse('users'))


@login_required
def all_friends(request):
    friends = Friend.objects.friends(request.user)

    return render(
        request,
        'friends.html',
        {
            'friends': friends
        }
    )