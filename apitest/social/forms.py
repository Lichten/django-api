from django import forms
from django.contrib.auth.models import User


class RegisterForm(forms.ModelForm):
    username = forms.CharField(label=u"Имя пользователя")
    first_name = forms.CharField(label=u"Имя")
    last_name = forms.CharField(label=u"Фамилия")
    email = forms.EmailField(label=u"Email", required=False)
    password = forms.CharField(label=u"Пароль", widget=forms.PasswordInput)
    password_confirm = forms.CharField(label=u"Подтвердите пароль", widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']

    def clean(self):
        super().clean()

        if self.data['password'] != self.data['password_confirm']:
            self.add_error("password_confirm", u"Пароли не совпадают")

        return self.data

    def save(self):
        super().save()
        user = User.objects.get(username=self.cleaned_data['username'])
        user.set_password(self.cleaned_data['password'])
        user.save()

        return user


class LoginForm(forms.Form):
    username = forms.CharField(label=u"Имя пользователя")
    password = forms.CharField(label=u"Пароль", widget=forms.PasswordInput)