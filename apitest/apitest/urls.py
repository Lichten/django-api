"""apitest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, include

from social.views import add_friend, all_users, index, register, all_friends


urlpatterns = [
    path('admin/', admin.site.urls),
    path('friendship/', include('friendship.urls')),
    path('login', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('registration', register, name='register'),
    path('users', all_users, name='users'),
    path('index', index, name='index'),
    path('add_friend/<slug:username>', add_friend, name='add_friend'),
    path('friends', all_friends, name='friends')
]
